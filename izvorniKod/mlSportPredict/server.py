#!/usr/bin/env python
# coding: utf-8

# # What Sport Will You Compete In?
# ## Based on your height, weight, age and gender, what sport are you most likely to compete in in the Olympic Games?



from http.server import HTTPServer, BaseHTTPRequestHandler

import numpy as np
import pandas as pd
import seaborn as sns
from imblearn.over_sampling import SMOTE
from sklearn.dummy import DummyClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import (
    classification_report,
    f1_score,
    make_scorer,
    precision_recall_fscore_support,
    precision_score,
    recall_score,
)
from sklearn.model_selection import (
    GridSearchCV,
    cross_validate,
    train_test_split,
)
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import LabelEncoder, MinMaxScaler
from sklearn.svm import LinearSVC
from sklearn.tree import DecisionTreeClassifier

import pickle

import json


RF = pickle.load(open("trainDataRF.dat", "rb"))
le = pickle.load(open("trainDatale.dat", "rb"))


class myHandler(BaseHTTPRequestHandler):


    def do_POST(self):
        # print(self.headers)

        age = self.headers["age"]
        height = self.headers["height"]
        weight = self.headers["weight"]
        year = 2021
        if(self.headers["Sex"]=="MALE"):
            sex_M = 1
        else:
            sex_M = 0


        self.data = {"Age": age, "Height": height, "Weight": weight, "Year": year, "Sex_M": sex_M}

        # print(self.headers)

        

        pred_event = RF.predict(pd.DataFrame([self.data]))
        # all_events = le.inverse_transform(np.arange(0, df["Sport"].nunique(), 1))
        all_probs = RF.predict_proba(pd.DataFrame([self.data]))[0]
        pred_proba = all_probs[pred_event]

        retSport = str(le.inverse_transform(pred_event)[0])

    #     print(
    #     "Your predicted sport for the "
    #     + str(data["Year"])
    #     + " Olympic Games is "
    #     + str(le.inverse_transform(pred_event)[0])
    #     + " with a probability of "
    #     + str(round(100 * pred_proba[0], 1))
    #     + "%"
    # )

        
        self.send_response(200)
        # self.send_header("sport",retSport)
        self.end_headers()

        self.wfile.write(retSport.encode("utf-8"))


        # print("data je --> ", self.data)





def main():

    PORT = 1234
    server = HTTPServer(('',PORT), myHandler)
    server.serve_forever()



    # data = {"Age": 24, "Height": 182, "Weight": 79, "Year": 2021, "Sex_M": 1}

    # pred_event = RF.predict(pd.DataFrame([data]))
    # all_events = le.inverse_transform(np.arange(0, df["Sport"].nunique(), 1))
    # all_probs = RF.predict_proba(pd.DataFrame([data]))[0]
    # pred_proba = all_probs[pred_event]
    # print(
    #     "Your predicted sport for the "
    #     + str(data["Year"])
    #     + " Olympic Games is "
    #     + str(le.inverse_transform(pred_event)[0])
    #     + " with a probability of "
    #     + str(round(100 * pred_proba[0], 1))
    #     + "%"
    # )
    


if __name__ == '__main__':
    main()


# In[ ]:




